from random import randint, choice
from time import time


def reviser():
    # num_table = randint(1, 10)
    count = 1
    num_table = "AA"
    while not num_table.isdigit() or len(num_table) != 1:
        num_table = input(f"Quelle table voulez vous réviser ? ")
    num_table = int(num_table)
    rep_OK = 0
    liste = list(range(1, 11))
    time_list = []
    while count < 11:
        num = choice(liste)
        liste.remove(num)
        res = num_table*num
        choix = "A"
        while not choix.isdigit():
            h = time()
            choix = input(f"{count}  /  {num_table} x {num} = ")
        time_list.append((round(time() - h, 2), count))
        choix = int(choix)
        if choix == res:
            rep_OK += 1
        count += 1

    if rep_OK == 10:
        print("Excellent !")
    elif rep_OK == 9:
        print("Trés bien.")
    elif 7 <= rep_OK <= 8:
        print("Bien.")
    elif 4 <= rep_OK <= 6:
        print("Moyen.")
    elif 1 <= rep_OK <= 3:
        print("Il faut retravailler cette table.")
    else:
        print("Est-ce que tu l’as fait exprès ?")
    print(f"Temps maxi de {sorted(time_list, reverse = True)[0][0]}s pour l'opération {num_table} x {sorted(time_list, reverse = True)[0][1]}",
          f"Temps moyen : {sum(time_list[0])/10}", sep="\n")
    continuer = input("On continue les révisions (o/n) ? ")
    if continuer == "n" or continuer == "N":
        return None
    reviser()


reviser()
